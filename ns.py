#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from os import devnull
from time import sleep
from inspect import stack
from re import search as rsearch
from requests import Session as reqSes
from urllib.parse import quote_plus
from subprocess import call, getoutput
from feedparser import parse as fpparse
from requests.auth import HTTPBasicAuth
from configparser import SafeConfigParser
from argparse import ArgumentParser, FileType
from socket import setdefaulttimeout as sdeftimeout
from logging import getLogger, INFO, DEBUG, Formatter, StreamHandler


class NyaaScraper(object):
    def __init__(self):
        try:
            parser = ArgumentParser(description='Process some integers.')
            parser.add_argument('-c', '--config', type=FileType('r'), required=True, help='Config .ini')
            parser.add_argument('-i', '--ip', required=False, help='Host name, IP Address')
            parser.add_argument('-w', '--wol', nargs='?', const='3:10', required=False, help='WoL?')
            parser.add_argument('-v', '--verbose', action='count', required=False, help='Host name, IP Address')
            args = parser.parse_args() #--add/del/list anime ?

            sdeftimeout(6)
            self.log = getLogger(__name__)
            if args.verbose:
                if args.verbose == 1:
                    level = INFO
                elif args.verbose >= 2:
                    level = DEBUG

                self.log.setLevel(level)
                handler = StreamHandler()
                handler.setLevel(level)
                formatter = Formatter('%(message)s')
                handler.setFormatter(formatter)
                self.log.addHandler(handler)
                self.log.debug('[$] Debug Mode: ON!')

            if args.config:
                if args.ip:
                    self.ip = args.ip
                if args.wol:
                    self.wol = [int(w) for w in args.wol.split(':')]
                else:
                    self.wol = None
                self.cfgf = args.config.name
                self.LoadCFG(self.cfgf)
        except Exception as ex:
            self.log.debug('[#] Exception @ %s (%s > %s)' % (stack()[0][3], type(ex).__name__, ex.args))


    def LoadCFG(self, file):
        try:
            self.cfg = SafeConfigParser()
            self.cfg.read(file)
            self.alst = {}

            for s in self.cfg.sections():
                if s == '_download_':
                    self.usr = self.cfg.get(s, 'usr')
                    self.pwd = self.cfg.get(s, 'pwd')
                    self.port = self.cfg.getint(s, 'port')
                    self.dlst = self.cfg.get(s, 'tids').split()
                    if self.ip:
                        self.CheckPC(self.ip, self.wol)
                else:
                    title = self.cfg.get(s, 'title')
                    ep = self.cfg.getint(s, 'ep')
                    sgroup = self.cfg.get(s, 'sgroup')
                    hdonly = self.cfg.getboolean(s, 'hdonly')
                    if ep <= 9:
                        ep = "0%i" % ep
                    self.alst[s] = [title, ' '+str(ep)+' ', sgroup, hdonly, {}]
            self.log.debug('[$] DL = %s' % self.dlst)
            self.log.debug('[$] AL = %s' % self.alst)
            self.log.info('[*] Loaded %i items.' % len(self.alst.keys()))
            self.AniCheck()
        except Exception as ex:
            self.log.debug('[#] Exception @ %s (%s > %s)' % (stack()[0][3], type(ex).__name__, ex.args))


    def SaveCFG(self, file):
        try:
            for s in self.cfg.sections():
                self.log.debug('[$] Saving changes for: %s' % s)
                if s == '_download_':
                    self.cfg.set(s, 'tids', ' '.join(self.dlst))
                else:
                    self.cfg.set(s, 'ep', self.alst[s][1])
            with open(self.cfgf, 'w') as ucfg:
                self.cfg.write(ucfg)
        except Exception as ex:
            self.log.debug('[#] Exception @ %s (%s > %s)' % (stack()[0][3], type(ex).__name__, ex.args))


    def AniCheck(self, tid=None):
        try:
            for a in self.alst.keys():
                self.log.info('[*] Searching for>  %s - %s' % (self.alst[a][0], self.alst[a][1].strip()))
                f = fpparse('http://www.nyaa.se/?page=rss&cats=1_37&term=%s' % quote_plus(' '.join(self.alst[a][:-2])))

                for ap in f.entries:
                    kwlst = self.alst[a][0].split()
                    kwlst.extend(self.alst[a][1:-2])
                    if all(kw.encode('utf-8') in ap.title.encode('utf-8') for kw in kwlst): #check if all keywords are in found element title
                        self.alst[a][4].update({ap.title.encode('utf-8'): ap.link})
                        self.log.debug('[$] Adding to array => %s - %s' % (ap.title.encode('utf-8'), ap.link))

                if self.alst[a][4]: #choosing the right key, if link(s) exist...
                    for key, value in self.alst[a][4].items(): #iterate through all links and pick best
                        if self.alst[a][3]: #if want only HD...
                            if '1080'.encode('utf-8') in key:
                                tid = value.split('=')[-1]
                                break
                            elif '720'.encode('utf-8') in key:
                                tid = value.split('=')[-1]
                                break
                        else: #if quality doesn't matters...
                            tid = value.split('=')[-1]
                            break
                if tid and tid not in self.dlst:
                    self.log.info('[+] Found: %s  (%s)' % (key.decode('utf-8'), value))
                    self.log.debug('[$] Found ID: %s' % tid)
                    self.alst[a][1] = str(int(self.alst[a][1]) + 1)
                    self.dlst.append(tid)
            if self.dlst and self.ip:
                self.CheckPC(self.ip, self.wol)
            self.SaveCFG(self.cfgf)
        except Exception as ex:
            self.log.debug('[#] Exception @ %s (%s > %s)' % (stack()[0][3], type(ex).__name__, ex.args))


    def CheckPC(self, ip=None, wakeup=None):
        try:
            dn = open(devnull, 'wb')
            pchk = call(['ping', '-c', '2', '-W', '2', ip], stdout=dn, stderr=dn)
            if pchk != 0: #if pc off
                self.log.info('[+] PC: %s is off.' % ip)
                if wakeup: #but we wanna wake him up
                    mchk = getoutput('arp -a | grep "(%s)" | cut -d" " -f4' % ip)
                    for l in range(wakeup[0]):
                        self.log.info('[+] Trying to wake up and checking (every %is)  %i/3' % (wakeup[1], l + 1))
                        mpkt = call(['ether-wake', mchk], stdout=dn, stderr=dn) #win/fail=always 0
                        sleep(wakeup[1])
                        pchk = call(['ping', '-c', '2', '-W', '2', ip], stdout=dn, stderr=dn)
                        if pchk == 0:
                            break
            if pchk == 0: #if PC is on
                self.uTDL(self.ip, self.port, self.usr, self.pwd)
        except Exception as ex:
            self.log.debug('[#] Exception @ %s (%s > %s)' % (stack()[0][3], type(ex).__name__, ex.args))


    def uTDL(self, ip=None, port=None, usr=None, pwd=None):
        try:
            url = 'http://%s:%s/gui/' % (ip, port)
            gauth = HTTPBasicAuth(usr, pwd)
            rs = reqSes()
            rtok = rs.get('%stoken.html' % url, auth=gauth, timeout=6)
            token = rsearch(r'<div[^>]*id=[\"\']token[\"\'][^>]*>([^<]*)</div>', rtok.text).group(1)
            for tid in self.dlst[:]:
                tpa = {'token' : token, 'action': 'add-url', 's': 'http://www.nyaa.se/?page=download&tid=%s' % tid}
                rtj = rs.get(url, params=tpa, auth=gauth, timeout=6)
                if rtj.status_code == 200:
                    self.dlst.remove(tid)
                    self.log.info('[+] Added uT job! (tid = %s)' % tid)
        except Exception as ex:
            self.log.debug('[#] Exception @ %s (%s > %s)' % (stack()[0][3], type(ex).__name__, ex.args))


if __name__ == '__main__':
    NyaaScraper()
